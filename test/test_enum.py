from nbudemo import MyEnum


def test_enums():
    # this returns an int
    A = MyEnum.A
    assert A == 0
    # you can build a true underlying enum type from the integer value
    assert MyEnum(A) == A
    assert repr(MyEnum(A)) == "A"
