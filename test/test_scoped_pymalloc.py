import numpy as np
from nbudemo import inspect_buffer_of_double


# this uses the buffer interface under the hood
def test_inspect_array():
    a = np.arange(10, dtype="d")
    inspect_buffer_of_double(a)
