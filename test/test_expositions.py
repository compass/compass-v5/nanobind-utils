import numpy as np
from nbudemo import X


def test_dtype():
    assert X.numpy_dtype == np.dtype([("data", "<f8", (3,))])


def test_as_bytes():
    x = X()
    assert x.as_bytes().shape == (3 * 8,)
