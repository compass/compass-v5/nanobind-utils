#include <compass_cxx_utils/pretty_print.h>
#include <nanobind_utils/as-bytes.h>
#include <nanobind_utils/enum.h>
#include <nanobind_utils/nanobind-utils.h>
#include <nanobind_utils/numpy-dtype.h>
#include <nanobind_utils/scoped-pymalloc.h>

#include <span>

namespace nb = nanobind;

enum struct MyEnum : char { A, B, C };

NB_MAKE_OPAQUE(MyEnum);

struct X {
  std::array<double, 3> data;
};

NB_MODULE(nbudemo, m) {

  m.def("steal", []() {
    using namespace compass::nanobind;
    std::vector<int> v{1, 2, 3, 4, 5};
    auto result = steal_buffer(v);
    assert(v.empty());
    return result;
  });

  m.def("reshape", []() {
    using namespace compass::nanobind;
    std::vector<int> v{1, 2, 3, 4};
    auto result = copy_buffer(v, shape(2));
    return result;
  });

  m.def("vector_of_array", []() {
    using namespace compass::nanobind;
    std::vector<std::array<int, 4>> v;
    v.push_back({1, 2, 3, 4});
    v.push_back({5, 6, 7, 8});
    return copy_buffer(v);
  });

  m.def("reshape_vector_of_array", []() {
    using namespace compass::nanobind;
    std::vector<std::array<int, 4>> v;
    v.push_back({1, 2, 3, 4});
    v.push_back({5, 6, 7, 8});
    return copy_buffer(v, shape(2));
  });

  m.def("copy", [](nb::ndarray<int, nb::c_contig> &a) {
    using namespace compass::nanobind;
    return copy_buffer(a.data(), a.shape(0));
  });

  compass::nanobind::bind_enum<MyEnum>(
      m, "MyEnum", {{MyEnum::A, "A"}, {MyEnum::B, "B"}, {MyEnum::C, "C"}});

  auto pyX = nb::class_<X>(m, "X");
  pyX.def(nb::init<>());
  compass::nanobind::add_numpy_dtype(pyX, {{"data", "double", {3}}});
  compass::nanobind::add_as_bytes(pyX);

  m.def(
      "inspect_buffer_of_double",
      [](nb::handle h, const bool ro) {
        compass::nanobind::scoped_pymalloc<Py_buffer> view;
        auto status = PyObject_GetBuffer(nb::steal(h).ptr(), view.get(),
                                         ro ? PyBUF_RECORDS_RO : PyBUF_RECORDS);
        if (status != 0)
          throw std::runtime_error("Could not get buffer!");
        auto p = view.get();
        assert(p->ndim == 1);
        const auto data = reinterpret_cast<const double *>(p->buf);
        using compass::utils::Pretty_printer;
        Pretty_printer print;
        print(std::span{data, data + p->shape[0]});
      },
      nb::arg().none(false), nb::arg("ro") = true);
}
