import importlib.metadata

__version__ = importlib.metadata.version("nbutils-demo")

import compass

with compass.nanobind():
    from .nbudemo import *
