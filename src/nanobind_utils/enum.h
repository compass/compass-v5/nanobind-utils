#pragma once

#include <map>
#include <string>
#include <type_traits>

#include <nanobind/nanobind.h>

namespace nb = nanobind;

namespace compass {
namespace nanobind {

template <typename E, typename... Args>
  requires(std::is_enum_v<E>)
// bind enum of a module with bind_enum(module, ...)
// or bind an inner enum of a class with bind_enum(cls, ...)
auto bind_enum(auto &holder, const std::string &name,
               const std::map<E, std::string> &entries) {
  auto cls = nb::class_<E>{holder, name.c_str()};
  cls.def("__init__", [](E *t, int val) { new (t) E{static_cast<E>(val)}; });
  for (auto &&[value, name] : entries) {
    cls.def_prop_ro_static(
        name.c_str(), [value](nb::handle) { return static_cast<int>(value); });
  }
  cls.def("__repr__", [entries](const E &self) {
    auto it = entries.find(self);
    if (it != entries.end())
      return it->second.c_str();
    return "unknown enum value";
  });
  cls.def("__eq__", [](const E &self, int v) {
    return static_cast<std::underlying_type_t<E>>(self) == v;
  });
  return cls;
}

} // namespace nanobind
} // namespace compass
