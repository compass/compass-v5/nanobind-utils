#pragma once

#include <nanobind/nanobind.h>

namespace nb = nanobind;

namespace compass {

namespace nanobind {

namespace detail {

struct dtype_spec {
  /** Atribute name */
  std::string name;
  /** Numpy dtype name */
  std::string dtype;
  /** Shape */
  std::vector<std::size_t> shape;
};

} // namespace detail

template <typename pyClass>
void add_numpy_dtype(pyClass &cls,
                     const std::vector<detail::dtype_spec> &specs) {
#ifndef NDEBUG
  if (nb::hasattr(cls, "numpy_dtype")) {
    std::cerr << "Overriding existing definition of numpy_dtype." << std::endl;
  }
#endif
  nb::module_ np = nb::module_::import_("numpy");
  nb::list l;
  for (auto &&[name, dtype, shape] : specs) {
    if (shape.empty()) {
      l.append(nb::make_tuple(name.c_str(), np.attr(dtype.c_str())));
    } else {
      nb::list sh;
      for (auto &&n : shape)
        sh.append(n);
      l.append(nb::make_tuple(name.c_str(), np.attr(dtype.c_str()), sh));
    }
  }
  auto dtype = np.attr("dtype")(l);
  cls.def_prop_ro_static("numpy_dtype", [dtype](nb::handle) { return dtype; });
}

} // namespace nanobind

} // namespace compass
