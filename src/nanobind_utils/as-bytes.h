#pragma once

#include <nanobind/nanobind.h>
#include <nanobind/ndarray.h>

namespace nb = nanobind;

namespace compass {

namespace nanobind {

template <typename pyClass, typename byte_type = unsigned char>
void add_as_bytes(pyClass &cls) {
#ifndef NDEBUG
  if (nb::hasattr(cls, "as_bytes")) {
    std::cerr << "Overriding existing definition of as_bytes." << std::endl;
  }
#endif
  using T = typename pyClass::Type;
  cls.def(
      "as_bytes",
      [](nb::handle h) {
        static_assert(sizeof(byte_type) == 1);
        return nb::ndarray<byte_type, nb::numpy>{
            reinterpret_cast<byte_type *>(nb::cast<T *>(h)), {sizeof(T)}, h};
      },
      nb::keep_alive<0, 1>());
}

} // namespace nanobind

} // namespace compass
