#pragma once

#include <algorithm>
#include <ranges>
#include <vector>

#include <nanobind/nanobind.h>
#include <nanobind/ndarray.h>

namespace nb = nanobind;

namespace compass {
namespace nanobind {

template <std::size_t n> using shape_ = std::array<size_t, n>;

template <typename... I>
  requires(std::conjunction_v<std::is_integral<I>...>)
constexpr inline auto shape(I &&...i) {
  return shape_<sizeof...(i)>{static_cast<size_t>(i)...};
}

template <std::size_t n>
constexpr inline auto prepend(const std::size_t i, shape_<n> s) {
  shape_<n + 1> result;
  result[0] = i;
  for (std::size_t k = 0; k < n; ++k)
    result[k + 1] = s[k];
  return result;
}

namespace detail {

template <typename T, std::size_t n>
  requires(n > 0)
struct Buffer_interface {
  T *data;
  shape_<n> shape;
};

template <std::size_t n>
constexpr inline std::size_t prod(const shape_<n> &sh) {
  std::size_t s = 1;
  for (auto &&i : sh)
    s *= i;
  return s;
}

template <typename T, std::size_t m, std::size_t k = 0>
inline auto buffer(std::array<T, m> *p, const std::size_t n,
                   const shape_<k> &elt_shape = {}) {
  assert(p);
  const auto elt_size = prod(elt_shape);
  assert(m % elt_size == 0);
  return Buffer_interface<T, k + 2>{
      p->data(), prepend(n, prepend(m / elt_size, elt_shape))};
}

template <typename T, std::size_t m, std::size_t k = 0>
inline auto buffer(const std::array<T, m> *p, const std::size_t n,
                   const shape_<k> &elt_shape = {}) {
  return buffer(const_cast<std::array<T, m> *>(p), n, elt_shape);
}

template <typename T, std::size_t k = 0>
inline auto buffer(T *p, const std::size_t n, const shape_<k> &elt_shape = {}) {
  assert(p);
  const auto elt_size = prod(elt_shape);
  assert(n % elt_size == 0);
  if constexpr (std::is_const_v<T>) {
    using TT = std::remove_const_t<T>;
    return Buffer_interface<TT, k + 1>{const_cast<TT *>(p),
                                       prepend(n / elt_size, elt_shape)};
  } else {
    return Buffer_interface<T, k + 1>{p, prepend(n / elt_size, elt_shape)};
  }
}

template <typename T, std::size_t n = 0>
inline auto buffer(std::vector<T> &v, const shape_<n> &elt_shape = {}) {
  return buffer(v.data(), v.size(), elt_shape);
}

template <typename T, std::size_t n, typename Owner = nb::handle>
inline auto as_array(const Buffer_interface<T, n> &buffer, Owner &&owner = {}) {
  return nb::ndarray<nb::numpy, T>(buffer.data, buffer.shape.size(),
                                   buffer.shape.data(), owner);
}

} // namespace detail

template <typename V, std::size_t n = 0>
inline auto steal_buffer(V &&v, const shape_<n> &elt_shape = {}) {
  using namespace detail;
  using T = typename std::decay_t<V>::value_type;
  std::vector<T> *thief = new std::vector<T>{std::move(v)};
  assert(thief);
  nb::capsule owner(reinterpret_cast<void *>(thief), [](void *p) noexcept {
    delete reinterpret_cast<std::vector<T> *>(p);
  });
  return as_array(buffer(*thief, elt_shape), owner);
}

template <typename T, std::size_t m = 0>
inline auto copy_buffer(const T *p, const std::size_t n,
                        const shape_<m> &elt_shape = {}) {
  assert(p);
  return steal_buffer(std::vector<T>{p, p + n}, elt_shape);
}

template <typename T, std::size_t n = 0>
inline auto copy_buffer(const std::span<T> &span,
                        const shape_<n> &elt_shape = {}) {
  return copy_buffer(span.data(), span.size(), elt_shape);
}

template <typename T, std::size_t n = 0>
inline auto copy_buffer(const std::vector<T> &v,
                        const shape_<n> &elt_shape = {}) {
  return copy_buffer(v.data(), v.size(), elt_shape);
}

template <typename T, std::size_t m = 0>
inline auto view_buffer(T *p, const std::size_t n,
                        const shape_<m> &elt_shape = {}) {
  using namespace detail;
  assert(p);
  return as_array(buffer(p, n, elt_shape));
}

template <typename T, std::size_t m = 0>
inline auto view_buffer(std::span<T> &span, const shape_<m> &elt_shape = {}) {
  return view_buffer(span.data(), span.size(), elt_shape);
}

template <typename T, std::size_t n = 0>
inline auto view_buffer(std::vector<T> &v, const shape_<n> &elt_shape = {}) {
  using namespace detail;
  return as_array(buffer(v, elt_shape));
}

template <typename T, typename TT = T>
inline auto as_1D_array(T *p, const std::size_t &n) {
  static_assert(sizeof(T) == sizeof(TT));
  return nb::ndarray<nb::numpy, TT>{p, {n}};
}

template <typename T, typename TT = T>
inline auto as_2D_array(T *p, const std::size_t &n, const std::size_t &m) {
  static_assert(sizeof(T) == sizeof(TT));
  return nb::ndarray<nb::numpy, TT>{p, {n, m}};
}

template <typename T, typename TT = T>
inline auto as_1D_array(std::vector<T> &v) {
  return as_1D_array<T, TT>(v.data(), v.size());
}

template <typename T, std::size_t n, typename TT = T>
inline auto as_array(std::array<T, n> &a) {
  return as_1D_array<T, TT>(a.data(), n);
}

template <typename T, std::size_t n, std::size_t m, typename TT = T>
inline auto as_array(std::array<std::array<T, m>, n> &a) {
  return as_2D_array<T, TT>(&a[0][0], n, m);
}

} // namespace nanobind
} // namespace compass
