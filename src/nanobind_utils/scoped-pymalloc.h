#pragma once

#include <nanobind/nanobind.h>

namespace nb = nanobind;

namespace compass {

namespace nanobind {

// this is a mere copy/paste from nanobind/nb_internals.h
template <typename T> struct scoped_pymalloc {
  scoped_pymalloc(size_t size = 1) {
    ptr = (T *)PyMem_Malloc(size * sizeof(T));
    if (!ptr)
      throw std::runtime_error("compass::nanobind::scoped_pymalloc failed");
  }
  ~scoped_pymalloc() { PyMem_Free(ptr); }
  T *release() {
    T *temp = ptr;
    ptr = nullptr;
    return temp;
  }
  T *get() const { return ptr; }
  T &operator[](size_t i) { return ptr[i]; }
  T *operator->() { return ptr; }

private:
  T *ptr{nullptr};
};

} // namespace nanobind

} // namespace compass
