#pragma once

#include <algorithm>
#include <ranges>
#include <vector>

#include <nanobind/nanobind.h>

namespace nb = nanobind;

namespace compass {
namespace nanobind {

namespace detail {

template <typename> struct Tuple_factory;

template <std::size_t... i> struct Tuple_factory<std::index_sequence<i...>> {
  template <typename T>
  static constexpr auto apply(const std::array<T, sizeof...(i)> &a) {
    return nb::make_tuple(T{a[i]}...);
  }
};

} // namespace detail

template <typename T, std::size_t n>
constexpr auto as_tuple(const std::array<T, n> &a) {
  return detail::Tuple_factory<std::make_index_sequence<n>>::apply(a);
}

} // namespace nanobind
} // namespace compass
